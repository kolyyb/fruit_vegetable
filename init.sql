DROP DATABASE IF EXISTS fruit_vegetable_data;
CREATE DATABASE IF NOT EXISTS fruit_vegetable_data;
USE fruit_vegetable_data;

CREATE TABLE produits
(
    id           INT AUTO_INCREMENT PRIMARY KEY,
    name         VARCHAR(65) UNIQUE,
    id_categorie INT
);

CREATE TABLE categories
(
    id   INT AUTO_INCREMENT primary key,
    type ENUM ('fruit', 'vegetables') UNIQUE
);

CREATE TABLE pays
(
    id   INT AUTO_INCREMENT primary key,
    name VARCHAR(65) UNIQUE
);

CREATE TABLE transac
(
    order_transac  INT,
    amount_transac INT,
    date_transac   DATE,
    id_pays        INT,
    id_produit     INT,
    FOREIGN KEY (id_pays) REFERENCES pays (id),
    FOREIGN KEY (id_produit) REFERENCES produits (id)
);
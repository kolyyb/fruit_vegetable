import pandas as pd
import numpy as np

df = pd.read_csv('pivot-tables.csv')


def get_product_by_country_category(country_name: list, category_name: list) -> pd.DataFrame:
    pivot_table_one_3_d = df.query(f'Country == {country_name} and Category == {category_name}').pivot_table(values='Amount', index=['Country','Category'], columns='Product',
                                                                             aggfunc='sum', margins = True, margins_name='Total results', dropna=False)
    return pivot_table_one_3_d

def get_product_by_country(country_name: list) -> pd.DataFrame:
    pivot_table_one_d = df.query(f'Country == {country_name}').pivot_table(values='Amount', index='Product', columns='Country', aggfunc='count',
                                       margins=True, margins_name='Total results')
    return pivot_table_one_d


if __name__ == '__main__':
    print("Produits par pays")
    print(get_product_by_country(['France', 'Germany', 'Australia']))
    print('-' * 50)
    print("Produits par pays et par categorie")
    print(get_product_by_country_category(['France'], ['Vegetables']))



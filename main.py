import dataclasses

import mysql.connector as mysql
import pandas as pd
from pandas import Timestamp


@dataclasses.dataclass
class Row:
    order_id: int
    product: str
    category: str
    amount: int
    date_origin: Timestamp
    country: str

    # conversion date
    def convert_slash_to_underscore(self):
        return self.date_origin.strftime("%Y-%m-%d")


list_data = []

df = pd.read_excel('pivot-tables.xlsx', sheet_name='Sheet1', engine='openpyxl')
# print(df.values)

for row_xlsx in df.values:
    row = Row(row_xlsx[0], row_xlsx[1], row_xlsx[2], row_xlsx[3], row_xlsx[4], row_xlsx[5])
    list_data.append(row)

conn = mysql.connect(host='localhost', user='root', password='pass', database='fruit_vegetable_data')
cur = conn.cursor(dictionary=True, prepared=True)


# def store_category():
#     try:
#         cur.execute("""INSERT INTO categories(type) VALUES('fruit'), ('vegetables');""")
#         conn.commit()
#     except:
#         pass


# Préparation à l'insertion des pays
def store_pays(pays_name: str):
    try:
        cur.execute("""INSERT INTO pays (name) VALUES (%s);""", (pays_name.lower(),))
        conn.commit()


    except:
        pass
    cur.execute("""SELECT id FROM pays WHERE name = %s;""", (pays_name.lower(),))
    id_pays = cur.fetchone()
    return id_pays["id"]

def store_categories(category_name:str):
    try:
        cur.execute("""INSERT INTO categories (type) VALUES (%s);""", (category_name.lower(),))
        conn.commit()
    except:
        pass


# Préparation : recuperation de l'id de la categorie du fichier excel puis on insert le produit avec l'id de la categorie associée
def store_products(product_name: str, categorie: str):
    try:
        cur.execute("""SELECT id FROM categories WHERE type = %s;""", (categorie.lower(),))
        id_category = cur.fetchone()

        cur.execute("""SELECT id FROM produits WHERE name = %s;""", (product_name.lower(),))
        id_product = cur.fetchone()

        if id_product is None:
            print(id_product, product_name, id_category['id'])
            cur.execute("""INSERT INTO produits (name, id_categorie) VALUES (%s, %s);""",
                    (product_name.lower(), id_category['id']))
            conn.commit()


    except TypeError as e:
        pass
    cur.execute("""SELECT id FROM produits WHERE name = %s;""", (product_name.lower(),))
    id_product = cur.fetchone()
    return id_product["id"]


def store_transac(order_transac:int, amount_transac:int, date_transac:str, id_pays:int, id_produit:int):
    try:
        cur.execute("""INSERT INTO transac (order_transac, amount_transac, date_transac, id_pays, id_produit) 
        VALUES (%s, %s, %s, %s, %s);""", (order_transac, amount_transac, date_transac, id_pays, id_produit))
        conn.commit()
    except:
        pass


# parcours des enregistrements du fichier excel ligne par ligne et insertion
for row in list_data:
    id_country_stored = store_pays(row.country) # enregistrement du pays et recuperation de son id
    store_categories(row.category)
    id_product_stored = store_products(row.product, row.category) # enregistrement du produit et recuperation de son id
    store_transac(row.order_id,
                  row.amount,
                  row.convert_slash_to_underscore(),
                  id_country_stored,
                  id_product_stored)
cur.close()

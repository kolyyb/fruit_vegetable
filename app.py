import json

import pandas as pd
from fastapi import FastAPI

app = FastAPI()

df = pd.read_csv('pivot-tables.csv')


def get_product_by_country_category(country_name: list, category_name: list) -> pd.DataFrame:
    pivot_table_one_3_d = df.query(f'Country == {country_name} and Category == {category_name}').pivot_table(
        values='Amount', index=['Country', 'Category'], columns='Product',
        aggfunc='sum', margins=True, margins_name='Total results', dropna=False)
    return json.loads(pivot_table_one_3_d.to_json(orient='columns'))


def get_product_by_country(country_name: list) -> pd.DataFrame:
    pivot_table_one_d = df.query(f'Country == {country_name}').pivot_table(values='Amount', index='Product',
                                                                           columns='Country', aggfunc='count',
                                                                           margins=True, margins_name='Total results')
    return json.loads(pivot_table_one_d.to_json(orient='columns'))


@app.get("/country={country}")
def get_product_by_country_route(country: str):
    return get_product_by_country([country])


@app.get("/category={category_name}&country={country}")
def get_product_by_country_category_route(category_name: str, country: str):
    return get_product_by_country_category([country],[category_name])
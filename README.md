# Brief commerce de fruits et légumes

- [x] **Analyser le fichier**
- [x] **Modeliser une BDD pour recupérer les données du fichier. + Etablir les règles de normalisation des données.**
- [x] **Mettre les données du fichier et les mettres dans la bdd. En les normalisants.**
- [x] **Avec pandas faire des tables équivalente aux TCD du fichier.**
- [x] **Faire des fonctions en python qui permettent de récupérer les tables pandas.**


- [x] **option:**
* faire une api qui permet d'appeler les fonctions.
+ mettre le résultat de votre travail sur un repo GIT. + rendre le travail pour mardi 27/02/2024 à 9h00 (modifié)